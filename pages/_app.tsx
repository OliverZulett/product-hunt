// import '../styles/globals.css'
import 'tailwindcss/tailwind.css';
import type { AppProps } from 'next/app';
import { FirebaseContext } from '../firebase';
import firebase from '../firebase/firebase';
import useAuthentication from '../hooks/useAuthentication';

function MyApp({ Component, pageProps }: AppProps) {
  const user = useAuthentication();

  return (
    <FirebaseContext.Provider value={{ firebase, user }}>
      <Component {...pageProps} />
    </FirebaseContext.Provider>
  );
}
export default MyApp;
