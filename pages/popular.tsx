import React from 'react';
import PropTypes from 'prop-types';
import Layout from '../components/Layout';

const Popular = () => {
  return (
    <>
      <title>Product Hunt - Popular</title>
      <Layout>
        <h1>Popular</h1>
      </Layout>
    </>
  );
};

export default Popular;
