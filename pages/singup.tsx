import { useCallback, useState } from 'react';
import Layout from '../components/Layout';
import TitlePage from '../components/TitlePage';
import useValidation from '../hooks/useValidation';
import validateAccount from '../validation/createAccountValidation';
import { User } from '../models/User';
import { UserError } from '../models/UserError';
import AlertError from '../components/AlertError';
import firebase from '../firebase/firebase';
import { useRouter } from 'next/router';
import validateAccountCreation from '../validation/createAccountValidation';

const INITIAL_STATE: User = {
  name: '',
  email: '',
  password: '',
  password2: '',
};

const SingUp = () => {
  const router = useRouter();
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const { values, errors, handleOnSubmit, handleOnChange, handleOnBlur } =
    useValidation(INITIAL_STATE, validateAccountCreation, singUp);

  const { errorName, errorEmail, errorPassword }: UserError = errors;
  const { name, email, password, password2 }: User = values;

  async function singUp() {
    try {
      await firebase.singUp(name || '', email, password);
      console.log('New user registered!!');
      setError(false);
      router.push('/');
    } catch (error: any) {
      console.error(`there was an error: ${error}`);
      setError(true);
      setErrorMessage(error.message);
    }
  }

  return (
    <Layout>
      <div className="container mx-auto">
        <TitlePage
          title={'Create Account'}
          subTitle={'You can create your account here.'}
        />
        <div className="mt-5 md:mt-0 md:col-span-2">
          {error && <AlertError errorMessage={errorMessage} />}
          <form onSubmit={handleOnSubmit}>
            <div className="shadow sm:rounded-md sm:overflow-hidden">
              <div className="px-4 py-5 bg-white space-y-6 sm:p-6">
                <div className="grid grid-cols-3 gap-6">
                  <div className="col-span-3 sm:col-span-2">
                    <label htmlFor="name" className="label-text">
                      Name
                    </label>
                    <div className="mt-1 flex rounded-md shadow-sm">
                      <input
                        type="text"
                        name="name"
                        id="name"
                        className={`input w-full ${
                          !errorName ? 'input-bordered' : 'input-error'
                        }`}
                        placeholder="name"
                        value={name}
                        onChange={handleOnChange}
                        onBlur={handleOnBlur}
                      />
                    </div>
                    {errorName && (
                      <p className="text-xs text-error mt-3"> {errorName} </p>
                    )}
                  </div>
                </div>

                <div className="grid grid-cols-3 gap-6">
                  <div className="col-span-3 sm:col-span-2">
                    <label htmlFor="email" className="label-text">
                      Email
                    </label>
                    <div className="mt-1 flex rounded-md shadow-sm">
                      <input
                        type="email"
                        name="email"
                        id="email"
                        className={`input w-full ${
                          !errorEmail ? 'input-bordered' : 'input-error'
                        }`}
                        placeholder="you@example.com"
                        value={email}
                        onChange={handleOnChange}
                        onBlur={handleOnBlur}
                      />
                    </div>
                    {errorEmail && (
                      <p className="text-xs text-error mt-3"> {errorEmail} </p>
                    )}
                  </div>
                </div>

                <div className="grid grid-cols-3 gap-6">
                  <div className="col-span-3 sm:col-span-2">
                    <label htmlFor="password" className="label-text">
                      Password
                    </label>
                    <div className="mt-1 flex rounded-md shadow-sm">
                      <input
                        type="password"
                        name="password"
                        id="password"
                        className={`input w-full ${
                          !errorPassword ? 'input-bordered' : 'input-error'
                        }`}
                        placeholder="your password"
                        value={password}
                        onChange={handleOnChange}
                        onBlur={handleOnBlur}
                      />
                    </div>
                    <p className="mt-2 text-sm text-gray-500">
                      Verify your password
                    </p>
                    <div className="mt-1 flex rounded-md shadow-sm">
                      <input
                        type="password"
                        name="password2"
                        id="password2"
                        className={`input w-full ${
                          !errorPassword ? 'input-bordered' : 'input-error'
                        }`}
                        placeholder="your password"
                        value={password2}
                        onChange={handleOnChange}
                        onBlur={handleOnBlur}
                      />
                    </div>
                    {errorPassword && (
                      <p className="text-xs text-error mt-3">
                        {' '}
                        {errorPassword}{' '}
                      </p>
                    )}
                  </div>
                </div>
              </div>
              <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                <button type="submit" className="btn btn-primary">
                  Create Account
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </Layout>
  );
};

export default SingUp;
