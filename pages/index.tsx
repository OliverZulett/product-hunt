import { getDocs, collection, query, orderBy } from 'firebase/firestore';
import type { NextPage } from 'next';
import React, { useContext, useEffect, useState } from 'react';
import Layout from '../components/Layout';
import FirebaseContext from '../firebase/context';
import ProductComponent from '../components/Product';
import { Product } from '../models/Product';
import TitlePage from '../components/TitlePage';
import Preloader from '../components/Preloader';

const Home: NextPage = () => {
  const [products, setProducts] = useState<any>([]);
  const { firebase } = useContext(FirebaseContext);

  useEffect(() => {
    const getProducts = async () => {
      const productsRef = collection(firebase.db, "products");
      const q = query(productsRef, orderBy('createdAt', 'desc'));
      const querySnapshot = await getDocs(q);
      const products: any[] = [];
      querySnapshot.forEach(doc => {
        products.push({ id: doc.id, ...doc.data() });
        console.log(`${doc.id} =>`, doc.data());
      });
      setProducts(products);
    };
    getProducts();
  }, []);

  
  if (products.length === 0) return (<Preloader/>)

  return (
    <Layout>
      <div className="container mx-auto">
        <TitlePage
          title={'Last Products'}
          subTitle={'You can find the last products here.'}
        />

        <div className="shadow sm:rounded-md sm:overflow-hidden">
          <div className="px-4 py-5 bg-white space-y-6 sm:p-6">
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 ">
              {products.map(
                ({
                  name,
                  company,
                  description,
                  imageUrl,
                  comments,
                  votes,
                  id,
                  url,
                  createdAt
                }: Product) => (
                  <ProductComponent
                    key={id}
                    id={id}
                    name={name}
                    company={company}
                    description={description}
                    imageUrl={imageUrl}
                    totalComments={comments?.length || 0}
                    votes={votes}
                    url={url}
                    createdAt={createdAt}
                  />
                ),
              )}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Home;