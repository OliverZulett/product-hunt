import React, { useState, FunctionComponent } from 'react';
import Layout from '../components/Layout';
import TitlePage from '../components/TitlePage';
import { useRouter } from 'next/router';
import firebase from '../firebase';
import useValidation from '../hooks/useValidation';
import { UserError } from '../models/UserError';
import AlertError from '../components/AlertError';
import { User } from '../models/User';
import singInAccountValidation from '../validation/singInAccountValidation';

const INITIAL_STATE: User = {
  email: '',
  password: '',
};

const Login: FunctionComponent = () => {
  const router = useRouter();
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const { values, errors, handleOnSubmit, handleOnChange, handleOnBlur } =
    useValidation(INITIAL_STATE, singInAccountValidation, singIn);

  const { errorEmail, errorPassword }: UserError = errors;
  const { email, password }: User = values;

  async function singIn() {
    try {
      const user = await firebase.singIn(email, password);
      console.log('User Log in!!');
      setError(false);
      router.push('/');
    } catch (error: any) {
      console.error(`there was an error: ${error}`);
      setError(true);
      setErrorMessage(error.message);
    }
  }

  return (
    <Layout>
      <div className="container mx-auto">
        <TitlePage title={'Log In'} subTitle={'Access to your account.'} />
        <div className="mt-5 md:mt-0 md:col-span-2">
          {error && <AlertError errorMessage={errorMessage} />}
          <form onSubmit={handleOnSubmit}>
            <div className="shadow sm:rounded-md sm:overflow-hidden">
              <div className="px-4 py-5 bg-white space-y-6 sm:p-6">
                <div className="grid grid-cols-3 gap-6">
                  <div className="col-span-3 sm:col-span-2">
                    <label htmlFor="email" className="label-text">
                      Email
                    </label>
                    <div className="mt-1 flex rounded-md shadow-sm">
                      <input
                        type="email"
                        name="email"
                        id="email"
                        className={`input w-full ${
                          !errorEmail ? 'input-bordered' : 'input-error'
                        }`}
                        placeholder="you@example.com"
                        value={email}
                        onChange={handleOnChange}
                        onBlur={handleOnBlur}
                      />
                    </div>
                    {errorEmail && (
                      <p className="text-xs text-error mt-3"> {errorEmail} </p>
                    )}
                  </div>
                </div>

                <div className="grid grid-cols-3 gap-6">
                  <div className="col-span-3 sm:col-span-2">
                    <label htmlFor="password" className="label-text">
                      Password
                    </label>
                    <div className="mt-1 flex rounded-md shadow-sm">
                      <input
                        type="password"
                        name="password"
                        id="password"
                        className={`input w-full ${
                          !errorPassword ? 'input-bordered' : 'input-error'
                        }`}
                        placeholder="your password"
                        value={password}
                        onChange={handleOnChange}
                        onBlur={handleOnBlur}
                      />
                    </div>
                    {errorPassword && (
                      <p className="text-xs text-error mt-3">
                        {' '}
                        {errorPassword}{' '}
                      </p>
                    )}
                  </div>
                </div>
              </div>
              <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                <button type="submit" className="btn btn-primary">
                  log in
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </Layout>
  );
};

export default Login;
