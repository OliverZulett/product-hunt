/* eslint-disable @next/next/no-img-element */
import React, {
  FunctionComponent,
  useCallback,
  useContext,
  useRef,
  useState,
} from 'react';
import Layout from '../components/Layout';
import TitlePage from '../components/TitlePage';
import { useRouter } from 'next/router';
import useValidation from '../hooks/useValidation';
import { FirebaseContext } from '../firebase';
import { Product } from '../models/Product';
import { ProductError } from '../models/ProductError';
import validateProductCreation from '../validation/createProductValidation';
import { addDoc, collection, setDoc } from '@firebase/firestore';
import { ref, uploadBytes, getDownloadURL } from '@firebase/storage';
import Alert from '../components/Alert';

const INITIAL_STATE: Product = {
  name: '',
  company: '',
  url: '',
  imageUrl: '',
  description: '',
};

interface Alert {
  status: boolean;
  message: string;
  type: 'info' | 'success' | 'warning' | 'error';
}

const CreateProduct: FunctionComponent = () => {
  const router = useRouter();
  const inputRef = useRef<any>(null);

  const [alert, setAlert] = useState<Alert>({
    status: false,
    message: '',
    type: 'info',
  });
  const [image, setImage] = useState<any>(null);
  const [submitProduct, setSubmitProduct] = useState(false);
  const { user, firebase } = useContext(FirebaseContext);

  const { values, errors, handleOnSubmit, handleOnChange, handleOnBlur } =
    useValidation(INITIAL_STATE, validateProductCreation, createProduct);
  const { errorName, errorCompany, errorUrl, errorDescription }: ProductError =
    errors;
  const { name, company, url, description }: Product = values;

  const handleOnSelectImage = useCallback(() => {
    const file = inputRef.current.files[0];
    if (file) {
      setImage(file);
      console.log('image ready to upload');
    } else {
      setAlert({
        status: true,
        message: `invalid image file`,
        type: 'error',
      });
    }
  }, []);

  async function createProduct() {
    if (!user) {
      setAlert({
        status: true,
        message: `Unauthorized user`,
        type: 'error',
      });
      return router.push('/login');
    }
    setSubmitProduct(true);
    const product: Product = {
      name,
      company,
      url,
      description,
      votes: 0,
      comments: [],
      createdAt: Date.now(),
      creator: {
        id: user.uid,
        name: user
      }
    };
    try {
      if (image) {
        const docRef = await addDoc(
          collection(firebase.db, 'products'),
          product,
        );
        console.log('Product created with ID: ', docRef.id);
        const imageUrl = await uploadImage(docRef.id);
        if (imageUrl) {
          console.log('image updaloaded', imageUrl);
          await setDoc(docRef, { imageUrl: imageUrl }, {merge: true})
            .then(() => {
              setAlert({
                status: true,
                message: `Product created successfully`,
                type: 'success',
              });
            })
            .catch(err => {
              console.error(`error updating data product: ${err}`);
              setAlert({
                status: true,
                message: `error updating data product: ${err.message}`,
                type: 'error',
              });
            });
        } else {
          setAlert({
            status: true,
            message: `product image did not load correctly`,
            type: 'warning',
          });
        }
        setTimeout(() => {
          setSubmitProduct(false);
          router.push('/');
        }, 4000);
      } else {
        setAlert({
          status: true,
          message: `image cannot be empty`,
          type: 'error',
        });
      }
      setSubmitProduct(false);
    } catch (err: any) {
      console.error('Error creating product: ', err);
      setAlert({
        status: true,
        message: `error uploading file: ${err.message}`,
        type: 'error',
      });
    }
  }

  const uploadImage = async (imageName: string) => {
    const reference = ref(firebase.storage, `products/${imageName}`);
    return uploadBytes(reference, image)
      .then(uploadResults => {
        console.log(uploadResults);
        return getDownloadURL(uploadResults.ref);
      })
      .catch(err => {
        console.error(`error uploading file: ${err}`);
        setAlert({
          status: true,
          message: `error uploading file: ${err.message}`,
          type: 'error',
        });
      });
  };

  console.log(user);
  

  return (
    <Layout>
      <div className="container mx-auto">
        <TitlePage
          title={'Create Product'}
          subTitle={'You can create a new product here.'}
        />
        <div className="my-5 md:mt-0 md:col-span-2">
          {alert.status && (
            <Alert
              message={alert.message}
              type={alert.type}
              timeToShow={5000}
            />
          )}
          
          <form onSubmit={handleOnSubmit}>
            <div className="shadow sm:rounded-md sm:overflow-hidden">
              <div className="px-4 py-5 bg-white space-y-6 sm:p-6">
                <h2 className="mb-5 text-2xl">General Information</h2>

                <div className="grid grid-cols-3 gap-6">
                  <div className="col-span-3 sm:col-span-2">
                    <label htmlFor="name" className="label-text">
                      Product Name
                    </label>
                    <div className="mt-1 flex rounded-md shadow-sm">
                      <input
                        type="text"
                        name="name"
                        id="name"
                        className={`input w-full ${
                          !errorName ? 'input-bordered' : 'input-error'
                        }`}
                        placeholder="name"
                        value={name}
                        onChange={handleOnChange}
                        onBlur={handleOnBlur}
                      />
                    </div>
                    {errorName && (
                      <p className="text-xs text-error mt-3"> {errorName} </p>
                    )}
                  </div>
                </div>

                <div className="grid grid-cols-3 gap-6">
                  <div className="col-span-3 sm:col-span-2">
                    <label htmlFor="company" className="label-text">
                      Company
                    </label>
                    <div className="mt-1 flex rounded-md shadow-sm">
                      <input
                        type="text"
                        name="company"
                        id="company"
                        className={`input w-full ${
                          !errorName ? 'input-bordered' : 'input-error'
                        }`}
                        placeholder="company"
                        value={company}
                        onChange={handleOnChange}
                        onBlur={handleOnBlur}
                      />
                    </div>
                    {errorCompany && (
                      <p className="text-xs text-error mt-3">{errorCompany}</p>
                    )}
                  </div>
                </div>

                <div className="grid grid-cols-3 gap-6">
                  <div className="col-span-3 sm:col-span-2">
                    <label htmlFor="url" className="label-text">
                      URL
                    </label>
                    <div className="mt-1 flex rounded-md shadow-sm">
                      <input
                        type="url"
                        name="url"
                        id="url"
                        className={`input w-full ${
                          !errorName ? 'input-bordered' : 'input-error'
                        }`}
                        placeholder="url"
                        value={url}
                        onChange={handleOnChange}
                        onBlur={handleOnBlur}
                      />
                    </div>
                    {errorUrl && (
                      <p className="text-xs text-error mt-3"> {errorUrl} </p>
                    )}
                  </div>
                </div>

                <div className="grid grid-cols-3 gap-6">
                  <div className="col-span-3 sm:col-span-2">
                    <div>
                      <label className="block text-sm font-medium text-gray-700">
                        Product photo
                      </label>
                      <div className="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                        <div className="space-y-1 text-center">
                          {(image && (
                            <div className="avatar">
                              <div className="mb-8 w-24 h-24 mask mask-squircle">
                                <img
                                  alt="uploaded image"
                                  src={URL.createObjectURL(image)}
                                />
                              </div>
                            </div>
                          )) || (
                            <svg
                              className="mx-auto h-12 w-12 text-gray-400"
                              stroke="currentColor"
                              fill="none"
                              viewBox="0 0 48 48"
                              aria-hidden="true"
                            >
                              <path
                                d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                                strokeWidth="2"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                              />
                            </svg>
                          )}
                          <div className="flex text-sm text-gray-600">
                            <label
                              htmlFor="file-upload"
                              className="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500"
                            >
                              <span className="">
                                {(image && 'Image selected: ') ||
                                  'Select an image'}
                              </span>
                              <input
                                id="file-upload"
                                name="file-upload"
                                type="file"
                                className="sr-only"
                                accept="image/*"
                                ref={inputRef}
                                onChange={handleOnSelectImage}
                              />
                            </label>
                            <p className="pl-1">
                              {(image && image.name) || 'no image selected yet'}
                            </p>
                          </div>
                          <p className="text-xs text-gray-500">
                            PNG, JPG, GIF up to 10MB
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <h2 className="mb-5 text-2xl">About the product</h2>

                <div className="grid grid-cols-3 gap-6">
                  <div className="col-span-3 sm:col-span-2">
                    <label htmlFor="description" className="label-text">
                      Description
                    </label>
                    <div className="mt-1 flex rounded-md shadow-sm">
                      <textarea
                        name="description"
                        id="description"
                        className={`input w-full h-24 ${
                          !errorName ? 'input-bordered' : 'input-error'
                        }`}
                        placeholder="description"
                        value={description}
                        onChange={handleOnChange}
                        onBlur={handleOnBlur}
                      ></textarea>
                    </div>
                    {errorDescription && (
                      <p className="text-xs text-error mt-3">
                        {errorDescription}
                      </p>
                    )}
                  </div>
                </div>
              </div>
              <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                <button
                  type="submit"
                  className="btn btn-primary"
                  disabled={Object.keys(errors).length > 0 || submitProduct}
                >
                  Create Product
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </Layout>
  );
};

export default CreateProduct;
function hola<T>(hola: any): [any, any] {
  throw new Error('Function not implemented.');
}
