import { NextPage } from 'next';
import React, { useContext, useEffect, useState } from 'react';
import Layout from '../../components/Layout';
import TitlePage from '../../components/TitlePage';
import { useRouter } from 'next/router';
import { FirebaseContext } from '../../firebase';
import Alert from '../../components/Alert';
import { Product } from '../../models/Product';
import { useCallback } from 'react';
import Preloader from '../../components/Preloader';
import { formatDistanceToNow } from 'date-fns';
import { enUS } from 'date-fns/locale';
import { doc, getDoc } from 'firebase/firestore';

interface Alert {
  status: boolean;
  message: string;
  type: 'info' | 'success' | 'warning' | 'error';
}

const ProductDetail: NextPage = () => {
  const router = useRouter();

  const { pid }: string | any = router.query;

  const [productError, setProductError] = useState(false);
  const [product, setProduct] = useState<Product | any>({});
  const [alert, setAlert] = useState<Alert>({
    status: false,
    message: '',
    type: 'info',
  });

  const { firebase } = useContext(FirebaseContext);

  useEffect(() => {
    if (pid) {
      const getProduct = async () => {
        const productRef = doc(firebase.db, 'products', pid);
        const productSnap = await getDoc(productRef);
        if (productSnap.exists()) {
          console.log('Product data:', productSnap.data());
          setProduct(productSnap.data());
        } else {
          console.log('No such product!');
          setProductError(true);
          setAlert({
            status: true,
            message: `no product found with id: ${pid}`,
            type: 'error',
          });
        }
      };
      getProduct();
    }
  }, [pid]);

  const handleOnBackHome = useCallback(() => {
    router.push('/');
  }, []);

  const {
    id,
    name,
    url,
    imageUrl,
    company,
    description,
    votes,
    comments,
    createdAt,
    creator,
  } = product;

  if (Object.keys(product).length === 0) return <Preloader />;

  return (
    <Layout>
      <div className="container mx-auto">
        <TitlePage
          title={productError ? 'product does not exists' : ''}
          subTitle={
            productError ? 'Go back to home to see another product' : ''
          }
        />
        <div className="my-5 md:mt-0 md:col-span-2">
          {alert.status && (
            <Alert
              message={alert.message}
              type={alert.type}
              timeToShow={5000}
            />
          )}
          <div className="shadow sm:rounded-md sm:overflow-hidden">
            <div className="px-4 py-5 bg-white space-y-6 sm:p-6">
              {productError && (
                <div className="flex justify-center items-center">
                  <button
                    className="btn btn-lg my-5"
                    onClick={handleOnBackHome}
                  >
                    Go back home
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-6 w-6 ml-3"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
                      />
                    </svg>
                  </button>
                </div>
              )}
              <div className="flex flex-col lg:flex-row">
                <div className="flex justify-center lg:w-2/4">
                  <div className="avatar lg:justify-center lg:items-start">
                    <div className="rounded-btn w-100 lg:w-3/4 h-80">
                      <img src={imageUrl} alt="" className="bordered" />
                    </div>
                  </div>
                </div>
                <div className="flex flex-col mx-2 lg:w-2/4 lg:items-start">
                  <div className="flex justify-between items-center mt-10 lg:mt-0">
                    <div>
                      <h1 className="text-3xl font-bold uppercase ">{name}</h1>
                    </div>
                    <div className="flex justify-center items-center ml-5">
                      <button>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="h-6 w-6"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"
                          />
                        </svg>
                      </button>
                      <span className="mx-1">{votes}</span>
                      <button>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="h-6 w-6"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"
                          />
                        </svg>
                      </button>
                    </div>
                  </div>
                  <div className="">
                    <span className="uppercase">{company}</span>
                  </div>
                  <div className="mt-5">
                    <p className="text-base">{description}</p>
                  </div>
                  <div className="mt-5 text-right">
                    <span className="italic text-base">
                      {`
                        Created At: 
                          ${formatDistanceToNow(new Date(createdAt), {
                            locale: enUS,
                          })} By ${creator.name || 'unknown'}
                      `}
                    </span>
                  </div>
                  <div className="mt-10 flex justify-center">
                    <button className="btn btn-wide btn-primary">
                      visit product
                    </button>
                  </div>

                  <div className="mt-10 w-full">
                    <div className="collapse w-100 ">
                      <input type="checkbox" />
                      <div className="collapse-title text-xl font-medium">
                        <button className="mr-3 flex">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-6 w-6 mr-2"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="2"
                              d="M8 10h.01M12 10h.01M16 10h.01M9 16H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-5l-5 5v-5z"
                            />
                          </svg>
                          <p>{comments?.length || 0} comments</p>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-6 w-6 ml-3"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="2"
                              d="M19 9l-7 7-7-7"
                            />
                          </svg>
                        </button>
                      </div>
                      <div className="collapse-content">
                        <div className="card my-5 shadow-lg compact side bg-base-100">
                          <div className="flex-row items-center space-x-4 card-body">
                            <div>
                              <div className="avatar">
                                <div className="rounded-full w-14 h-14 shadow">
                                  <img src="https://i.pravatar.cc/500?img=32" />
                                </div>
                              </div>
                            </div>{' '}
                            <div>
                              <h2 className="card-title">Janis Johnson</h2>{' '}
                              <p className="text-base-content text-opacity-40">
                                Accounts Agent
                              </p>
                            </div>
                          </div>
                        </div>

                        <div className="card my-5 shadow-lg compact side bg-base-100">
                          <div className="flex-row items-center space-x-4 card-body">
                            <div className="w-full">
                              <h2 className="card-title">Leave a comment</h2>{' '}
                              <div className="mt-5 flex rounded-md shadow-sm">          
                                <textarea
                                  name="description"
                                  id="description"
                                  className="input w-full h-24 input-bordered"
                                  placeholder="description"
                                ></textarea>
                              </div>
                              <button className="btn btn-primary mt-5">send comment</button> 
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default ProductDetail;
