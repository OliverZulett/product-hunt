import type { NextPage } from 'next';
import styled from 'styled-components';
import Layout from '../components/Layout';

const Heading = styled.h1`
  color: green;
`;

const About: NextPage = () => {
  return (
    <Layout>
      <Heading>About</Heading>
    </Layout>
  );
};

export default About;
