module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [require('daisyui'), require('@tailwindcss/forms')],
  daisyui: {
    themes: [
      {
        'product-hunt-theme': {
          'primary': '#80ED99',
          'primary-focus': '#64b477',
          'primary-content': '#ffffff',
          'secondary': '#57CC99',
          'secondary-focus': '#429471',
          'secondary-content': '#ffffff',
          'accent': '#38A3A5',
          'accent-focus': '#287071',
          'accent-content': '#ffffff',
          'neutral': '#22577A',
          'neutral-focus': '#1f4661',
          'neutral-content': '#ffffff',
          'base-100': '#ffffff',
          'base-200': '#f9fafb',
          'base-300': '#6e9bb9',
          'base-content': '#22577A',
          'info': '#22577A',
          'success': '#57CC99',
          'warning': '#FFB344',
          'error': '#E05D5D',
        },
      },
    ],
  },
};
