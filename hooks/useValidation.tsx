/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from 'react';

const useValidation = (initialState: any, validate: Function, fn: Function) => {
  const [values, setValues] = useState(initialState);
  const [errors, setErrors] = useState({});
  const [submitForm, setSubmitForm] = useState(false);

  useEffect(() => {
    if (submitForm) {
      const hasErrors = Object.keys(errors).length > 0;
      if (!hasErrors) {
        fn();
      }
      setSubmitForm(false);
    }
  }, [errors]);

  const handleOnChange = useCallback(
    e => {
      setValues({
        ...values,
        [e.target.name]: e.target.value,
      });
    },
    [values],
  );

  const handleOnSubmit = useCallback(
    e => {
      e.preventDefault();
      const errorsValidated = validate(values);
      setErrors(errorsValidated);
      setSubmitForm(true);
    },
    [values],
  );

  const handleOnBlur = useCallback(() => {
    const errorsValidated = validate(values);
    setErrors(errorsValidated);
  }, [values]);

  return {
    values,
    errors,
    submitForm,
    handleOnSubmit,
    handleOnChange,
    handleOnBlur,
  };
};

export default useValidation;
