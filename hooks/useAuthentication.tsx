import { onAuthStateChanged } from 'firebase/auth';
import { useState, useEffect } from 'react';
import firebaseApp from '../firebase/firebase';

const useAuthentication = () => {
  const [userAuthenticated, setUserAuthenticated] = useState<any>(null);

  useEffect(() => {
    const unSubscribe = onAuthStateChanged(firebaseApp.auth, user =>
      user ? setUserAuthenticated(user) : setUserAuthenticated(null),
    );
    return () => unSubscribe();
  }, []);

  return userAuthenticated;
};

export default useAuthentication;
