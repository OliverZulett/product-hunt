import { User } from '../models/User';
import { UserError } from '../models/UserError';

export default function validateAccountCreation(userData: User) {
  let errors: UserError = {};

  if (!userData.name) errors.errorName = 'Name is required';

  if (!userData.email) {
    errors.errorEmail = 'Email is required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(userData.email)) {
    errors.errorEmail = 'Email is invalid';
  }

  if (!userData.password || !userData.password2) {
    errors.errorPassword = 'Password is required';
  } else if (userData.password.length < 6 || userData.password2.length < 6) {
    errors.errorPassword = 'Password is too short';
  } else if (userData.password !== userData.password2) {
    errors.errorPassword = 'Passwords must be equal';
  }

  return errors;
}
