import { User } from '../models/User';
import { UserError } from '../models/UserError';

export default function singInAccountValidation(userData: User) {
  let errors: UserError = {};

  if (!userData.email) {
    errors.errorEmail = 'Email is required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(userData.email)) {
    errors.errorEmail = 'Email is invalid';
  }

  if (!userData.password) {
    errors.errorPassword = 'Password is required';
  } else if (userData.password.length < 6) {
    errors.errorPassword = 'Password is too short';
  }

  return errors;
}
