import { Product } from '../models/Product';
import { ProductError } from '../models/ProductError';

export default function validateProductCreation(productData: Product) {
  let errors: ProductError = {};

  if (!productData.name) errors.errorName = 'Name is required';

  if (!productData.company) errors.errorCompany = 'Company is required';

  if (!productData.url) {
    errors.errorUrl = 'URL is required';
  } else if (!/^(ftp|http|https):\/\/[^ "]+$/.test(productData.url)) {
    errors.errorUrl = 'invalid URL';
  }

  if (!productData.description)
    errors.errorDescription = 'Product about required';

  return errors;
}
