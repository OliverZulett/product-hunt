import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import {
  Auth,
  createUserWithEmailAndPassword,
  getAuth,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  signOut,
  updateProfile,
} from 'firebase/auth';
import { getFirestore, Firestore } from 'firebase/firestore';
import { FirebaseStorage, getStorage } from '@firebase/storage';
import firebaseConfig from './config';

class Firebase {
  public app: any;
  public auth: Auth;
  public db: Firestore;
  public storage: FirebaseStorage;

  constructor() {
    this.app = firebase.initializeApp(firebaseConfig);
    this.auth = getAuth(this.app);
    this.db = getFirestore(this.app);
    this.storage = getStorage(this.app);
  }

  async singUp(name: string, email: string, password: string) {
    return createUserWithEmailAndPassword(this.auth, email, password).then(
      userCredential => {
        const user = userCredential.user;
        updateProfile(user, {
          displayName: name,
        });
      },
    );
  }

  async singIn(email: string, password: string) {
    return signInWithEmailAndPassword(this.auth, email, password).then(
      userCredential => userCredential.user,
    );
  }

  async singOut() {
    await signOut(this.auth);
  }

  getAuthenticatedUser() {
    return onAuthStateChanged(this.auth, (user: any) => (user && user) || null);
  }
}

const firebaseApp = new Firebase();
export default firebaseApp;
