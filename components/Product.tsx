import { FunctionComponent } from 'react';
import React from 'react';
import { Product } from '../models/Product';
import { formatDistanceToNow } from 'date-fns';
import es from 'date-fns/esm/locale/es';
import { enUS } from 'date-fns/locale';
import Link from 'next/link';

interface ProductProps extends Product {
  totalComments: number;
}

const ProductComponent: FunctionComponent<ProductProps> = ({
  id,
  name,
  company,
  description,
  imageUrl,
  totalComments,
  votes,
  url,
  createdAt,
}) => {
  return (
    <div className="card shadow-xl image-full m-5 text-justify">
      <figure>
        <img src={imageUrl} />
      </figure>
      <div className="justify-end card-body">
        <h2 className="card-title font-bold uppercase">{name}</h2>
        <span className="mb-1 text-right">{company}</span>
        <p className="text-xs">{description}</p>
        <div className="flex my-2">
          <button className="mr-3">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M8 10h.01M12 10h.01M16 10h.01M9 16H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-5l-5 5v-5z"
              />
            </svg>
          </button>
          <div>
            <p>{totalComments} comments</p>
          </div>
        </div>
        <div className="card-actions flex justify-between items-center mt-5">
          <Link href={`/products/${id}`}>
            <button className="btn btn-primary">View details</button>
          </Link>
          <div className="flex content-center">
            <button>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"
                />
              </svg>
            </button>
            <span className="mx-1">{votes}</span>
            <button>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"
                />
              </svg>
            </button>
          </div>
        </div>
        <span className="italic text-xs text-right mt-2">
          {formatDistanceToNow(new Date(createdAt), { locale: enUS })}
        </span>
      </div>
    </div>
  );
};

export default ProductComponent;
