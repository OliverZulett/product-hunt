import React from 'react';
import { FunctionComponent } from 'react';

interface TitlePageProps {
  title: string;
  subTitle?: string;
}

const TitlePage: FunctionComponent<TitlePageProps> = ({ title, subTitle }) => {
  return (
    <div className="m-5">
      <h1 className="text-2xl font-bold leading-7 text-gray-900 sm:text-4xl sm:truncate">
        {title}
      </h1>
      {subTitle && <p className="text-gray-400">{subTitle}</p>}
    </div>
  );
};

export default TitlePage;
