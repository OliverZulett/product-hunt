import React from 'react';
import Navbar from './Navbar';
type HeaderProps = {};

const Header: React.FunctionComponent<HeaderProps> = (props: HeaderProps) => {
  return <Navbar />;
};

export default Header;
