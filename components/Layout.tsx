import React from 'react';
import Header from './Header';

type LayoutProps = {
  children: React.ReactElement;
};

const Layout: React.FunctionComponent<LayoutProps> = (props: LayoutProps) => {
  return (
    <>
      <Header></Header>

      <main>{props.children}</main>

      <footer></footer>
    </>
  );
};

export default Layout;
