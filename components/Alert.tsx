import { Fragment, FunctionComponent, useEffect, useState } from 'react';

enum alertType {
  info = 'alert-info',
  success = 'alert-success',
  warning = 'alert-warning',
  error = 'alert-error',
}

interface AlertProps {
  message: string;
  type: keyof typeof alertType;
  timeToShow?: number;
}

const Alert: FunctionComponent<AlertProps> = ({
  message,
  type,
  timeToShow = 5000,
}) => {
  const [displayAlert, setDisplayAlert] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setDisplayAlert(false);
    }, timeToShow);
  }, []);

  return (
    <Fragment>
      {displayAlert && (
        <div className={`alert my-5 ${alertType[type]}`}>
          <div className="flex-1">
            {(type === 'info' && (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor">
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                />
              </svg>
            )) ||
              (type === 'success' && (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor">
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                  />
                </svg>
              )) ||
              (type === 'warning' && (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor">
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"
                  />
                </svg>
              )) ||
              (type === 'error' && (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor">
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M18.364 18.364A9 9 0 005.636 5.636m12.728 12.728A9 9 0 015.636 5.636m12.728 12.728L5.636 5.636"
                  />
                </svg>
              ))}
            <label className="ml-2">{message}</label>
          </div>
        </div>
      )}
    </Fragment>
  );
};

export default Alert;
