import { FunctionComponent, useContext, useCallback } from 'react';
import Link from 'next/link';
import { FirebaseContext } from '../firebase';
import { Router, useRouter } from 'next/router';

interface NavbarProps {}

const Navbar: FunctionComponent<NavbarProps> = () => {
  const router = useRouter();
  const { user, firebase } = useContext(FirebaseContext);
  const handleOnSingOut = useCallback(async () => {
    await firebase.singOut();
    console.log('log out');
    router.push('/');
  }, []);
  return (
    <nav className="navbar mb-2 shadow-lg bg-primary text-neutral-content">
      <div className="navbar-start">
        <div className="hidden lg:flex">
          <Link href="/" passHref>
            <button>
              <h1 className="text-2xl font-bold ml-3">Product Hunt</h1>
            </button>
          </Link>
        </div>
        <div className="lg:hidden dropdown">
          <div tabIndex={0} className="btn btn-square btn-ghost">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-7 w-7"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M4 6h16M4 12h16M4 18h16"
              />
            </svg>
          </div>
          <ul
            tabIndex={0}
            className="p-2 mt-3 shadow menu dropdown-content rounded-box w-auto bg-base-200 text-gray-600"
          >
            <li className="">
              <Link href="/" passHref>
                <a className={`${router.pathname == '/' ? 'bg-primary' : ''}`}>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6 mr-2"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
                    />
                  </svg>
                  Home
                </a>
              </Link>
            </li>
            <li>
              <Link href="/popular" passHref>
                <a
                  className={`${
                    router.pathname == '/popular' ? 'bg-primary' : ''
                  }`}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6 mr-2"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z"
                    />
                  </svg>
                  Popular
                </a>
              </Link>
            </li>
            <li>
              <Link href="/about" passHref>
                <a
                  className={`${
                    router.pathname == '/about' ? 'bg-primary' : ''
                  }`}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6 mr-2"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M19 20H5a2 2 0 01-2-2V6a2 2 0 012-2h10a2 2 0 012 2v1m2 13a2 2 0 01-2-2V7m2 13a2 2 0 002-2V9a2 2 0 00-2-2h-2m-4-3H9M7 16h6M7 8h6v4H7V8z"
                    />
                  </svg>
                  About
                </a>
              </Link>
            </li>
            {user && (
              <li>
                <Link href="/create-product" passHref>
                  <a
                    className={`${
                      router.pathname == '/' ? 'create-product' : ''
                    }`}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-6 w-6 mr-2"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M17 14v6m-3-3h6M6 10h2a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2zm10 0h2a2 2 0 002-2V6a2 2 0 00-2-2h-2a2 2 0 00-2 2v2a2 2 0 002 2zM6 20h2a2 2 0 002-2v-2a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2z"
                      />
                    </svg>
                    Create Product
                  </a>
                </Link>
              </li>
            )}
            <li>
              <div className="flex mt-2 justify-center">
                <div className="flex-1 lg:flex-none">
                  <div className="form-control">
                    <input
                      type="text"
                      placeholder="Search"
                      className="input input-primary input-bordered"
                    />
                  </div>
                </div>
                <div className="flex-none">
                  <button className="btn btn-square btn-ghost">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      className="inline-block w-6 h-6 stroke-current"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                      ></path>
                    </svg>
                  </button>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <div className="md:hidden">
          <h1 className="text-xl font-bold">Product Hunt</h1>
        </div>
      </div>
      <div className="navbar-center">
        <div className="hidden md:flex lg:hidden">
          <h1 className="text-xl font-bold">Product Hunt</h1>
        </div>
        <div className="hidden lg:flex items-stretch">
          <Link href="/" passHref>
            <button
              className={`btn btn-ghost  rounded-btn ${
                router.pathname == '/' ? 'btn-active' : ''
              }`}
            >
              Home
            </button>
          </Link>
          <Link href="/popular" passHref>
            <button
              className={`btn btn-ghost  rounded-btn ${
                router.pathname == '/popular' ? 'btn-active' : ''
              }`}
            >
              Popular
            </button>
          </Link>
          <Link href="/about" passHref>
            <button
              className={`btn btn-ghost  rounded-btn ${
                router.pathname == '/about' ? 'btn-active' : ''
              }`}
            >
              About
            </button>
          </Link>
          {user && (
            <Link href="/create-product" passHref>
              <button
                className={`btn btn-ghost  rounded-btn ${
                  router.pathname == '/create-product' ? 'btn-active' : ''
                }`}
              >
                Create Product
              </button>
            </Link>
          )}
        </div>
      </div>
      <div className="navbar-end">
        <div className="hidden lg:flex">
          <div className="flex-1 lg:flex-none">
            <div className="form-control">
              <input
                type="text"
                placeholder="Search"
                className="input text-gray-900"
              />
            </div>
          </div>
          <div className="flex-none">
            <button className="btn btn-square btn-ghost">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                className="inline-block w-6 h-6 stroke-current"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                ></path>
              </svg>
            </button>
          </div>
        </div>
        <div tabIndex={0} className="btn btn-square btn-ghost">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"
            />
          </svg>
        </div>
        <div tabIndex={0} className="btn btn-square btn-ghost">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"
            />
          </svg>
        </div>
        <div className="dropdown dropdown-end">
          <div tabIndex={0} className="btn btn-square btn-ghost">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-8 w-8"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"
              />
            </svg>
          </div>
          <ul
            tabIndex={0}
            className="p-2 mt-3 shadow menu dropdown-content rounded-box w-52 bg-base-200 text-gray-600"
          >
            {(!user && (
              <>
                <li>
                  <Link href="/login" passHref>
                    <a
                      className={`${
                        router.pathname == '/login' ? 'bg-primary' : ''
                      }`}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6 mr-2"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth="2"
                          d="M11 16l-4-4m0 0l4-4m-4 4h14m-5 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h7a3 3 0 013 3v1"
                        />
                      </svg>
                      Log In
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/singup" passHref>
                    <a
                      className={`${
                        router.pathname == '/singup' ? 'bg-primary' : ''
                      }`}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6 mr-2"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth="2"
                          d="M18 9v3m0 0v3m0-3h3m-3 0h-3m-2-5a4 4 0 11-8 0 4 4 0 018 0zM3 20a6 6 0 0112 0v1H3v-1z"
                        />
                      </svg>
                      Sing Up
                    </a>
                  </Link>
                </li>
              </>
            )) || (
              <li>
                <a onClick={handleOnSingOut}>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6 mr-2"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"
                    />
                  </svg>
                  Log Out
                </a>
              </li>
            )}
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
