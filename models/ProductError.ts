export interface ProductError {
  errorName?: string;
  errorCompany?: string;
  errorUrl?: string;
  errorDescription?: string;
}
