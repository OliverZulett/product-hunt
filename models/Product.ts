export interface Product {
  id?: string;
  name: string;
  company: string;
  url: string;
  imageUrl?: string;
  description: string;
  votes?: number;
  comments?: Array<any>;
  createdAt?: any;
  creator?: {
    id: string;
    name: string;
  }
}
