export interface Alert {
  status: boolean;
  message: string;
  type: 'info' | 'success' | 'warning' | 'error';
}