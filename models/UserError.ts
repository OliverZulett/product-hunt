export interface UserError {
  errorName?: string;
  errorEmail?: string;
  errorPassword?: string;
}
